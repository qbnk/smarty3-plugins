<?php

/**
 * Try-Catch statement plugin for smarty.
 * @author Björn Hjortsten
 * @copyright QBNK
 *
 * Sample usage
 * <code>
 * {try}
 * 		{* Do something potentially dangerous *}
 * {catch}
 * </code>
 *
 * The above code will catch everything descending from \Exception and just not output anything if an exception occurs.
 * It is possible to specify the specific exception class via the optional parameter "exception".
 *
 * Sample usage of the exception parameter
 * <code>
 * {try}
 * 		{* Do something potentially dangerous *}
 * {catch exception="MyCustomException"}
 * </code>
 *
 * It is also possible to provide a string which is output if an exception occurs via the optional parameter "default".
 *
 * Sample usage of the default parameter
 * <code>
 * {try}
 * 		{* Do something potentially dangerous *}
 * {catch default="Something dangerous happened"}
 * </code>
 *
 * The above code will output the string "Something dangerous happened" if an exception occurs.
 */
class Smarty_Compiler_Try {
	public function compile(array $params, $smarty)
	{
		return <<<EOT
<?php
\$tryCatchLevel = ob_get_level();
ob_start();
try {
?>
EOT;
	}

}

class Smarty_Compiler_Catch {

	public function compile(array $params, $smarty)
	{
		$default = $this->getParameterRecursive('default', $params);
		if ($default !== '') {
			$default = 'echo ' . $default . ';';
		}

		$exception = $this->getParameterRecursive('exception', $params);
		if ($exception === '') {
			$exception = '\Exception';
		}
		return <<<EOT
<?php
	echo ob_get_clean();
} catch ($exception \$e) {
	while (\$tryCatchLevel < ob_get_level()) {
		ob_get_clean();
	}
	$default
}
?>
EOT;
	}

	protected function getParameterRecursive($needle, $haystack) {
		if (!is_array($haystack)) {
			return $haystack === $needle ? $haystack : '';
		} else {
			foreach ($haystack as $key => $item) {
				if (is_array($item)) {
					return $this->getParameterRecursive($needle, $item);
				} else {
					return $key === $needle ? $item : '';
				}
			}
		}
		return '';
	}
}